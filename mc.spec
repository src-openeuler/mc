Name:		mc
Summary:	a feature rich full-screen text mode application
Version:	4.8.33
Release:	1
Epoch:		1
License:	GPL-3.0-or-later
URL:		https://www.midnight-commander.org/
Source0:	http://ftp.midnight-commander.org/%{name}-%{version}.tar.xz

Patch0:		%{name}-spec.syntax.patch
Patch1:		%{name}-default_setup.patch
Patch2:		%{name}-tmpdir.patch

BuildRequires:  gcc e2fsprogs-devel glib2-devel gpm-devel groff-base
BuildRequires:  libssh2-devel >= 1.2.5 perl-generators pkgconfig slang-devel
BuildRequires:  make python3-boto3

%description
GNU Midnight Commander is a visual file manager, licensed under GNU General Public
License and therefore qualifies as Free Software. It's a feature rich full-screen
text mode application that allows you to copy, move and delete files and whole
directory trees, search for files and run commands in the subshell. Internal viewer
and editor are included.

Midnight Commander is based on versatile text interfaces, such as Ncurses or S-Lang,
which allows it to work on a regular console, inside an X Window terminal, over SSH
connections and all kinds of remote shells.

%package python
Summary:	Midnight Commander s3+ and UC1541 EXTFS backend scripts
BuildArch:	noarch
Requires:	%{name} = %{epoch}:%{version}-%{release}
Requires:	python3-boto3

%description python
Midnight Commander s3+ and UC1541 EXTFS backend scripts.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
sed -i "s,PREV_MC_VERSION=\"unknown\",PREV_MC_VERSION=\"%{version}\"," version.sh
%configure \
        PYTHON=%{__python3} \
	CFLAGS="%{optflags} -Wno-strict-aliasing" \
	--enable-vfs-sfs \
	--disable-vfs-sftp \
	--enable-vfs-ftp \
	--enable-charset \
	--enable-largefile \
	--enable-vfs-cpio \
	--enable-vfs-extfs \
	--enable-vfs-shell \
	--enable-vfs-tar \
	--disable-rpath \
	--with-x \
	--with-gpm-mouse \
	--with-screen=slang \
	%{nil}
%make_build

%install
mkdir -p  ${RPM_BUILD_ROOT}%{_sysconfdir}/profile.d

%make_install

install contrib/mc.{sh,csh} ${RPM_BUILD_ROOT}%{_sysconfdir}/profile.d

%find_lang %{name} --with-man

%files -f %{name}.lang
%license doc/COPYING
%{_sysconfdir}/profile.d/*
%dir %{_sysconfdir}/%{name}
%config(noreplace) %{_sysconfdir}/%{name}/*
%{_bindir}/*
%dir %{_libexecdir}/mc
%attr(755,root,root) %{_libexecdir}/mc/cons.saver
%{_libexecdir}/mc/ext.d
%dir %{_libexecdir}/mc/extfs.d
%{_libexecdir}/mc/extfs.d/README
%{_libexecdir}/mc/extfs.d/README.extfs
%{_libexecdir}/mc/extfs.d/a+
%{_libexecdir}/mc/extfs.d/apt+
%{_libexecdir}/mc/extfs.d/audio
%{_libexecdir}/mc/extfs.d/bpp
%{_libexecdir}/mc/extfs.d/changesetfs
%{_libexecdir}/mc/extfs.d/deb
%{_libexecdir}/mc/extfs.d/deba
%{_libexecdir}/mc/extfs.d/debd
%{_libexecdir}/mc/extfs.d/dpkg+
%{_libexecdir}/mc/extfs.d/gitfs+
%{_libexecdir}/mc/extfs.d/hp48+
%{_libexecdir}/mc/extfs.d/iso9660
%{_libexecdir}/mc/extfs.d/lslR
%{_libexecdir}/mc/extfs.d/mailfs
%{_libexecdir}/mc/extfs.d/patchfs
%{_libexecdir}/mc/extfs.d/patchsetfs
%{_libexecdir}/mc/extfs.d/rpm
%{_libexecdir}/mc/extfs.d/rpms+
%{_libexecdir}/mc/extfs.d/trpm
%{_libexecdir}/mc/extfs.d/u7z
%{_libexecdir}/mc/extfs.d/uace
%{_libexecdir}/mc/extfs.d/ualz
%{_libexecdir}/mc/extfs.d/uar
%{_libexecdir}/mc/extfs.d/uarc
%{_libexecdir}/mc/extfs.d/uarj
%{_libexecdir}/mc/extfs.d/ucab
%{_libexecdir}/mc/extfs.d/uha
%{_libexecdir}/mc/extfs.d/ulha
%{_libexecdir}/mc/extfs.d/ulib
%{_libexecdir}/mc/extfs.d/unar
%{_libexecdir}/mc/extfs.d/urar
%{_libexecdir}/mc/extfs.d/uwim
%{_libexecdir}/mc/extfs.d/uzip
%{_libexecdir}/mc/extfs.d/uzoo
%{_libexecdir}/mc/*.csh
%{_libexecdir}/mc/*.sh
%{_libexecdir}/mc/shell
%{_datadir}/%{name}

%files python
%{_libexecdir}/mc/extfs.d/s3+
%{_libexecdir}/mc/extfs.d/torrent
%{_libexecdir}/mc/extfs.d/uc1541

%files help
%{_mandir}/man1/*
%license doc/COPYING
%doc doc/FAQ doc/NEWS doc/README

%changelog
* Thu Jan 23 2025 Funda Wang <fundawang@yeah.net> - 1:4.8.33-1
- update to 4.8.33
- use MC_TMPDIR to specify tmpdir (upstream #4535, #4575)

* Mon Oct 28 2024 Funda Wang <fundawang@yeah.net> - 1:4.8.32-1
- update to 4.8.32

* Mon Jul 01 2024 yaoxin <yao_xin001@hoperun.com> - 1:4.8.31-1
- Update to 4.8.31
- Fixes:
  * FTBFS on FreeBSD with ext2fs attribute support
  * Broken stickchars (-a) mode
  * Wrong timestamp after resuming of file copy operation
  * Editor: wrong deletion of marked column
- Core:
  * Minimal version of GLib is 2.32.0
- Misc:
  * Skins: add color for non-printable characters in editor
- VFS:
  * fish: drop support of native FISH server and protocol. Rename VFS to shell

* Wed Apr 19 2023 wangkai <13474090681@163.com> - 1:4.8.29-1
- Update to 4.8.29

* Wed Jul 20 2022 yaoxin <yaoxin30@h-partners.com> - 1:4.8.28-1
- Update to 4.8.28 to fix CVE-2021-36370

* Tue Nov 19 2019 caomeng<caomeng5@huawei.com> - 1:4.8.21-3
- Package init
